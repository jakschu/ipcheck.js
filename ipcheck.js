"use strict";

var myIps = [
    "1.1.1.1",
    "192.168.178.1",
    "11111111.11111111.11111111.11111111",
    "1111111.1111111.1111111.1111111",
    "11111111111111111111111111111111",
    "111111111111111111111111111111111",
    "1111111111111111111111111111111",
    "10.1.3.1",
    "123.123.123.123",
    "123123123123",
    "abcabcabcabc",
    "255.255.255.256",
    "234.234.234.2341"
];

// window.alert("Please open your DevTools' Console")

var ipRE = new RegExp(/(([0,1]{8}\.?){4})|(((25[0-5])|(2[0-4][0-9])|([01]?[0-9]?[0-9]))\.){3}((25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9]))/);

var MAX = 255;

var checkIp = function(_ip){
    var validIP = ipRE.exec(_ip);
    if (validIP != null){
        if (validIP.input == validIP[0]){
            // input is a valid IP adress
            return true;
        } else {

            console.log(validIP.input, "detected as", validIP[0]);
            return false;
        }
    } else {
        return false;
    }
}

var ipGen = function(){
    // Check the whole IPV4 range for valid IP Adresses 
    let i, j, k, l =0;
    let ipStr = "";
    for (i=0; i<=MAX; i++){
        console.log(i);
        for (j=0; j <=MAX; j++){
            for (k=0; k <= MAX; k++){
                for (l=0; l <= MAX; l++){
                    ipStr = i+"."+j+"."+k+"."+l;
                    if (checkIp(ipStr) == false){
                        console.log(ipStr, "is not a vaild ip.");
                    }
                }
            }
        }
    }
}

for (var m=0; m < myIps.length; m++){
    let ipStr = "";
    ipStr = myIps[m];
    if (checkIp(ipStr) == false){
        console.log(ipStr, "is not a vaild ip.");
    } else {
        console.log(ipStr, "is a vaild ip.");
    }
}

var renderLine = function(_txt, _class){
    var para = document.createElement("p");
    para.innerText = _txt;
    para.classList.add(_class);
    document.body.appendChild(para);
}

var checkInput = function(){
    console.log("Checking Input");
    var inputEl = document.getElementsByTagName("input")[0];
    var valid = checkIp(inputEl.value);
    if (valid == true){
        console.log(inputEl.value, "is a vaild IP Adress");
        renderLine(inputEl.value + " is a vaild IP Adress", "green");
    } else {
        console.log(inputEl.value, "is not a vaild IP Adress!");
        renderLine(inputEl.value + " is not a vaild IP Adress", "red");
    }
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Warning! ipGen checks the entire IPV4 range for valid IP Adressess. This will take a long time and might bring down your Browser tab/window if run inside a Browser!  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */ 

//ipGen();
